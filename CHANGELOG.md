# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.2] - 2019-02-28
### Changed
- Update to v0.2.0.1293

## [0.2.1] - 2018-10-27
### Added
- First release, contains Radarr 0.2.0

[Unreleased]: ../../src
[0.2.2]: ../../branches/compare/v0.2.1..v0.2.2#diff
[0.2.1]: ../../src/v0.2.1
