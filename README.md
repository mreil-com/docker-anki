# docker-anki

[![](https://img.shields.io/bitbucket/pipelines/mreil-com/docker-anki.svg)](https://bitbucket.org/mreil-com/docker-anki/addon/pipelines/home)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> [Anki](https://apps.ankiweb.net/) image with 
> [AnkiConnect](https://foosoft.net/projects/anki-connect/) plugin

Based on [mreil/ubuntu-base][ubuntu-base].


## Usage

    docker run -i -t \
    -p 8765:8765 \
    mreil/anki


## Build

    ./gradlew build


## Releases

See [hub.docker.com](https://hub.docker.com/r/mreil/anki/tags/).


## License

see [LICENSE](LICENSE)


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-anki
