#!/usr/bin/env bash

set -ex

export DISPLAY=:99
export ANKICONNECT_BIND_ADDRESS=0.0.0.0

ANKI_PREFS=~/.local/share/Anki2

# Install prefs DB to make sure first startup succeeds without any installation prompt
mkdir -p ${ANKI_PREFS}
unzip /runtime/prefs.zip -d ${ANKI_PREFS}

# Install AnkiConnect
ANKI_CONNECT_DIR=${ANKI_PREFS}/addons21/2055492159
mkdir -p ${ANKI_CONNECT_DIR}
cp -pv /opt/anki-connect/plugin/* ${ANKI_CONNECT_DIR}
# Fix bind address, this takes precedence over env var
sed -i "s/127.0.0.1/${ANKICONNECT_BIND_ADDRESS}/" ${ANKI_CONNECT_DIR}/config.json


# Start Xvfb
Xvfb "${DISPLAY}" &
anki
