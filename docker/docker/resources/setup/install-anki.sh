#!/usr/bin/env bash

set -ex


# Install dependencies
apt-get update -y
apt-get install -y --no-install-recommends \
  python3 \
  git \
  anki \
  xvfb \
  unzip

# Install AnkiConnect
git clone https://github.com/FooSoft/anki-connect "${ANKI_CONNECT_DIR}"

# Uninstall unneded stuff
apt-get remove -y git

# Create user
useradd -m anki
